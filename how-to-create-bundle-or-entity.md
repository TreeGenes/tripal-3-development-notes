I think it would be useful for anyone developing Tripal related bundles/entities so I'm sharing this with all in here as well @channel

Creating an actual Tripal Content Type (bundle/entity) can be done using the code via https://gitlab.com/TreeGenes/tripal_contact_profile/blob/master/tripal_contact_profile.admin.inc (edited) 
Lines 45-54 in particular.

If you execute this, a new Tripal Contact Profile bundle gets created under the admin -> structure -> tripal content types
It will contain pretty much no fields most probably

For fields to appear in the bundle, one must first create a dummy record if no data exists in the tables - this is the ideal situation
To initiate a record, check out line 78
That just creates the object/variable... use this $record to add fields to it but just before you do that, make sure you create cvterms for each field (edited) 
This can be done for example using lines 82-87
that example is creating a cvterm for firstname

Now actually add data to the $record for this firstname field using lines 91-97
Repeat the above process for each field you want in the bundle

Then you can go to the bundle via the admin UI and click check for new fields and the fields should appear
You can also do this programmatically using lines 376-378
So this procedure is basically the same thing to create any record (except you wouldn't have to recreate the cvterms since that is a one time thing you have to do at the start of creating a bundle)